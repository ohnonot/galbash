# galbash - Create pure HTML galleries with bash

If you call this script without arguments, it will look for images in the 
current folder, create thumbnails & HTML files in subfolders $thumbs and $data
and an index.html in the current folder.

Thus, the image files are in the gallery's base folder, still easily accessible
by other means, and everything else is neatly tucked away.

## DEPENDENCIES

- imagemagick's convert
- grep, cat
- bash


## ANNOTATIONS

If there is a file called annotations in the same folder as the original images,
it will be parsed to add (short) annotations to the individual images' HTML
files.
The format is as follows:
imagefilename.ext: Some Annotation Text
Meaning:
At the immediate beginning of the line (no leading whitespace), the name of the
file, followed by a colon ':' (again, no space), followed by a single space.
Everything after that until the end of the line is the annotation.

## CSS

The repository provides a gallery.css file, and I highly recommend you use it!
The resulting HTML does not use any javascript, and the CSS is an integral part
of the functionality.
The script will be looking for ~/.config/galbash/gallery.css and copy it to the
appropriate folder, but you can also do that manually.

## ALL OPTIONS

    -s dir     the source directory
               Default: $source
    
    -d dir     the destination directory (will be created)
               Default: $dest
    
    -b string  where the back button will take the visitor
               Default: $back
    
    -t int     Size of thumbnails in pixels. minimum: 10
               Default: $thumbsize
    
    -f string  the format extension for thumbnails
               Default: $thumbformat
    
    -F string  where the favicon is
    
    -T string  The title of the gallery.
               Default: $title
    
    -n         Do not wrap from last image to first and vice versa
               Default: false, i.e. wrapping is enabled
    
    -N         Do not create an index page with thumbnails
               Default: false, i.e. a thumbpage is created
    
    -k         Create symbolic links instead of copying the original files.
               This has no effect if source and destination are the same.
               Default: Copy
    
    -h         Display this help

## REPO

https://notabug.org/ohnonot/galbash
https://framagit.org/ohnonot/galbash
